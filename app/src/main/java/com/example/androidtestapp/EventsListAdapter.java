package com.example.androidtestapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EventsListAdapter extends RecyclerView.Adapter<EventsListAdapter.EventsListViewHolder> {

    // Declare an ArrayList that will contain all SoundObjects
    private ArrayList<EventObject> eventObjects;

    AlertDialog alertDialog;


    // Demand all needed informations for the RecyclerView
    public EventsListAdapter(ArrayList<EventObject> eventObjects) {

        // Hand over all data to the private ArrayList
        this.eventObjects = eventObjects;
    }

    // Initialises each RecyclerView item
    @Override
    public EventsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Set the default design for a element in the RecyclerView that is based on sound_item.xml
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_items, null);

        // Returns a new ViewHolder for each RecyclerView item
        return new EventsListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventsListViewHolder holder, int position) {

        // Get a SoundObject from the ArrayList that also contains
        // Simplifies the set processes
        final EventObject object = this.eventObjects.get(position);
        // Set the name of each sound button that is represented by the (SoundObject)object
        holder.itemTitle.setText(object.getItemTitle());
        holder.itemDesc.setText(object.getItemDescriptions());
        byte[] imgByte = object.getItemImage();
        if(imgByte != null)
        {
            holder.itemImg.setImageBitmap(BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length));
        }

    }

    // Tells the RecyclerView how many items are accessible to be displayed
    // Should return the size of the given content provider (here: SoundObject ArrayList)
    @Override
    public int getItemCount() {
        return this.eventObjects.size();
    }

    // Gets all accessible areas that are declared by you
    public class EventsListViewHolder extends RecyclerView.ViewHolder {

        // TextView to display the name of a sound button
        TextView itemTitle, itemDesc;
        ImageView itemImg;

        public EventsListViewHolder(View itemView) {
            super(itemView);

            // Assign itemTextView to the TextView item declared in sound_item.xml
            itemTitle = (TextView) itemView.findViewById(R.id.itemTitle);
            itemDesc = (TextView) itemView.findViewById(R.id.itemDesc);
            itemImg = (ImageView) itemView.findViewById(R.id.itemImg);

            itemView.setOnClickListener(view -> {
                int position = getAdapterPosition();
                final EventObject object = eventObjects.get(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext());
                LayoutInflater inflater = (LayoutInflater)itemView.getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View mView = inflater.inflate(R.layout.view_event_dialog, null);
                String selectedDate = object.getItemDate();
                Date parsedDate = new Date();
                try {
                    parsedDate = new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                ((TextView) mView.findViewById(R.id.itemTitle)).setText(object.getItemTitle());
                ((TextView) mView.findViewById(R.id.itemDate)).setText( "Date : " + new SimpleDateFormat("EEE - MMM. dd, yyyy").format(parsedDate));
                ((TextView) mView.findViewById(R.id.itemDesc)).setText(object.getItemDescriptions());
                byte[] imgByte = object.getItemImage();
                Bitmap bitmap = null;
                if(imgByte != null)
                {
                    bitmap = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
                }
                ((ImageView) mView.findViewById(R.id.itemImg)).setImageBitmap(bitmap);
                Button btnClose = (Button) mView.findViewById(R.id.btnClose);
                btnClose.setOnClickListener(v ->{
                    alertDialog.dismiss();
                });
                builder.setView(mView);
                alertDialog = builder.create();
                alertDialog.show();
            });
        }
    }
}