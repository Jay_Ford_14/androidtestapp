package com.example.androidtestapp;

public class EventObject {

    private String itemTitle, itemDate, itemDescriptions;

    private  byte[] itemImage;

    public EventObject(String itemTitle, String itemDate, String itemDecriptions, byte[] itemImage){

        this.itemTitle = itemTitle;
        this.itemDate = itemDate;
        this.itemDescriptions = itemDecriptions;
        this.itemImage = itemImage;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public String getItemDate() {
        return itemDate;
    }

    public String getItemDescriptions() {
        return itemDescriptions;
    }

    public byte[] getItemImage() {
        return itemImage;
    }
}
