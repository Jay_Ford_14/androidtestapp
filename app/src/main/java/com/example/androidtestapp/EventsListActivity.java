package com.example.androidtestapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventListener;

public class EventsListActivity extends AppCompatActivity {

    int RESULT_LOAD_IMG = 1;
    byte[] imgData = null;
    String selectedDate, formattedDate;

    AlertDialog alertDialog;

    RecyclerView recyclerView;
    FloatingActionButton floatingActionBtn;

    ArrayList<EventObject> eventList = new ArrayList<>();
    EventsListAdapter eventsListAdapter = new EventsListAdapter(eventList);

    ImageView imgView;

    DatabaseHandler databaseHandler = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_list);
        selectedDate = getIntent().getExtras().getString("selectedDate");
        Date parsedDate = new Date();
        try {
            parsedDate = new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        formattedDate = new SimpleDateFormat("EEE - MMM. dd, yyyy").format(parsedDate);
        getSupportActionBar().setTitle(formattedDate);
        addDataToArrayList();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setAdapter(eventsListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        floatingActionBtn = (FloatingActionButton) findViewById(R.id.floatingActionBtn);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && floatingActionBtn.getVisibility() == View.VISIBLE) {
                    floatingActionBtn.hide();
                } else if (dy < 0 && floatingActionBtn.getVisibility() != View.VISIBLE) {
                    floatingActionBtn.show();
                }
            }
        });
        floatingActionBtn.setOnClickListener( view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View mView = getLayoutInflater().inflate(R.layout.create_event_dialog, null);

            ((TextView) mView.findViewById(R.id.txtSelectedDate)).setText( "Date : " + formattedDate );
            Button btnSave = (Button) mView.findViewById(R.id.btnSave);
            Button btnChoosePhoto = (Button) mView.findViewById(R.id.btnChoosePhoto);
            imgView = (ImageView) mView.findViewById(R.id.imgView);
            btnChoosePhoto.setOnClickListener(v ->{
                startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), RESULT_LOAD_IMG);
            });
            btnSave.setOnClickListener(v ->{
                String title = ((EditText) mView.findViewById(R.id.editTitle)).getText().toString();
                String desc = ((EditText) mView.findViewById(R.id.editDesc)).getText().toString();
                long test =  databaseHandler.createEvent(title, selectedDate, desc, imgData);
                if(test > 0)
                {
                    addDataToArrayList();
                    Toast.makeText(getApplicationContext(), "Event Saved.", Toast.LENGTH_SHORT).show();
                    imgData = null;
                }else{
                    Toast.makeText(getApplicationContext(), "Saving failed.", Toast.LENGTH_SHORT).show();
                }
                alertDialog.dismiss();
                Log.d("test", test + " ");

            });
            builder.setView(mView);
            alertDialog = builder.create();
            alertDialog.show();
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && data != null){
            Uri selectedImg = data.getData();
            imgView.setImageURI(selectedImg);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImg);
                imgData = getBitmapAsByteArray(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addDataToArrayList(){

        eventList.clear();

        // Get a cursor filled with all information from the MAIN_TABLE
        Cursor cursor = databaseHandler.getEventCollectionByDate(selectedDate);

        // Check if the cursor is empty or failed to convert the data
        if (cursor.getCount() == 0){

            Log.e("log", "Cursor is empty or failed to convert data");
            cursor.close();
        }

         // Add each item of MAIN_TABLE to soundList and refresh the RecyclerView by notifying the adapter about changes
        while (cursor.moveToNext() ){

            eventList.add(new EventObject(  cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getBlob(4)));
            eventsListAdapter.notifyDataSetChanged();
        }

        cursor.close();
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

}
