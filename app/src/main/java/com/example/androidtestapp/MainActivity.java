package com.example.androidtestapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.marcohc.robotocalendar.RobotoCalendarView;
import com.marcohc.robotocalendar.RobotoCalendarView.RobotoCalendarListener;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements RobotoCalendarListener {

    private RobotoCalendarView robotoCalendarView;

    DatabaseHandler databaseHandler = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("");

        // Gets the calendar from the view
        robotoCalendarView = findViewById(R.id.robotoCalendarPicker);

        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());

        markEvents();

    }

    public void markEvents()
    {
        // Get a cursor filled with all information from the MAIN_TABLE
        Cursor cursor = databaseHandler.getEventCollection();

        // Check if the cursor is empty or failed to convert the data
        if (cursor.getCount() == 0){

            Log.e("log", "Cursor is empty or failed to convert data");
            cursor.close();
        }

        // Add each item of MAIN_TABLE to soundList and refresh the RecyclerView by notifying the adapter about changes
        while (cursor.moveToNext() ){
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(cursor.getString(2));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                robotoCalendarView.markCircleImage1(calendar.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        cursor.close();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onDayClick(Date date) {
        Intent intent = new Intent(this, EventsListActivity.class);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateTime = dateFormat.format(date);
        intent.putExtra("selectedDate", dateTime);
        startActivity(intent);
    }

    @Override
    public void onDayLongClick(Date date) {
        //Toast.makeText(this, "onDayLongClick: " + date, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRightButtonClick() {
        //Toast.makeText(this, "onRightButtonClick!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLeftButtonClick() {
        //Toast.makeText(this, "onLeftButtonClick!", Toast.LENGTH_SHORT).show();
    }

}
