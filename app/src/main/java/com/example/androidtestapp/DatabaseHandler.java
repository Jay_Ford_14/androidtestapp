package com.example.androidtestapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

    // Define a tag that is used to log any kind of error or comment
    private static final String LOG_TAG = "DATABASEHANDLER";

    // Define a database name and version
    private static final String DATABASE_NAME = "testapp.db";
    private static final int DATABASE_VERSION = 1;

    // MAIN_TABLE contains all sounds for the soundboard
    // Define information about the main table
    private static final String TABLE_NAME = "tbl_events";

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_DESCRIPTION = "descriptions";
    private static final String COLUMN_IMG = "img";


    // Define the SQL statements to create both tables
    private static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_TITLE + " TEXT, " + COLUMN_DATE + " TEXT," + COLUMN_DESCRIPTION + " TEXT," + COLUMN_IMG + " BLOB);";

    // Create a constructor to start an instance of DatabaseHandler that will create the database
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(LOG_TAG, "Database successfully initialised: " + getDatabaseName());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            // Execute the creation statements
            db.execSQL(SQL_CREATE_TABLE);

        } catch (Exception e) {

            Log.e(LOG_TAG, "Failed to create: " + e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // We are using the app version instead of the database version to upgrade the database, so this method is unnecessary
        // If it gets called somehow it should only delete the main table that will be refilled again in the SoundboardActivity
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public long createEvent(String title, String date, String desc, byte[] img){

        SQLiteDatabase database = this.getWritableDatabase();

        try {

            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_TITLE, title);
            contentValues.put(COLUMN_DATE, date);
            contentValues.put(COLUMN_DESCRIPTION, desc);
            contentValues.put(COLUMN_IMG, img);
            // Insert the SoundObject into the MAIN_TABLE
            return database.insert(TABLE_NAME, null, contentValues);// Put the information into a ContentValues object

        } catch (Exception e) {

            Log.e(LOG_TAG, "(MAIN) Failed to insert sound: " + e.getMessage());
        } finally {

            database.close();
        }

        return 0;

    }

    public Cursor getEventCollectionByDate(String date) {

        // Get a readable instance of the database
        SQLiteDatabase database = this.getReadableDatabase();

        return database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_DATE + " = '" + date + "'", null);
    }

    public Cursor getEventCollection() {

        // Get a readable instance of the database
        SQLiteDatabase database = this.getReadableDatabase();

        return database.rawQuery("SELECT * FROM " + TABLE_NAME + " GROUP BY " + COLUMN_DATE, null);
    }


}